import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Routes from './src/routes.js';
import * as Font from 'expo-font';

export default function App() {

  async function loadFonts() {
    await Font.loadAsync({
      'FuturaBT-Medium': require('./src/assets/fonts/futura-bt.ttf')
    });
  }

  return (
    <>
      <StatusBar barStyle='dark-content' backgroundColor='transparent' />
      <Routes />
    </>
  );
}

