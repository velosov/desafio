import React from 'react';
import { ImageBackground, StyleSheet, Text, SafeAreaView, View } from 'react-native';
import { height, width } from '../constants/dimensions.js';
import HomeButton from './HomeButton.js';

export default function CardH1({title}) {
    switch(title) {
        case('HOT TRENDS'):
            return (
                <View style={styles.hotTrends}>
                    <ImageBackground source={require('../assets/Home/Rectangle-1.png')} resizeMode= 'stretch' style={{width: styles.hotTrends.width, height: styles.hotTrends.height}}>
                        <Text style={styles.titleHT}>{title}</Text>
                        <HomeButton />
                    </ImageBackground>
                </View>
            )
            break;
        case('JAQUETAS'):
            return (
                <View style={styles.jaquetas}>
                    <ImageBackground source={require('../assets/Home/Rectangle-2.png')} resizeMode= 'stretch' style={{width: styles.jaquetas.width, height: styles.jaquetas.height}}>
                        <Text style={styles.titleJ}>{title}</Text>
                        <HomeButton />
                    </ImageBackground>
                </View>
            )
            break;
        case('PREVIEW'):
            return (
                <View style={styles.preview}>
                    <ImageBackground source={require('../assets/Home/Rectangle.png')} resizeMode= 'stretch' style={{width: styles.preview.width, height: styles.preview.height}}>
                        <Text style={styles.titleP}>{title}</Text>
                        <HomeButton />
                    </ImageBackground>
                </View>
            )
            break;
    }
}

const styles = StyleSheet.create({
    hotTrends: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 192,
        height: 270,
        marginLeft: 16,
        color: '#FAFAFA',
    },
    titleHT: {
        marginBottom: 3,
        alignSelf: 'center',
        letterSpacing: 6.67,
        marginTop: 126,
        textAlign: 'center', 
        height: 16,
        color: '#FAFAFA',
        letterSpacing: 1.4,
        fontFamily: 'FuturaBT-Medium' ,
        width: 157
    },
    jaquetas: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 192,
        height: 270,
        marginLeft: 16,
        color: '#FAFAFA',
    },
    titleJ: {
        alignSelf: 'center',
        marginBottom: 3, 
        letterSpacing: 6.67,
        marginTop: 126,
        textAlign: 'center', 
        height: 17,
        color: '#FAFAFA',
        letterSpacing: 1.4,
        fontFamily: 'FuturaBT-Medium' ,
        width: 135,
    },
    preview: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: 343,
        height: 424,
        marginTop: -40,
        color: '#FAFAFA',
        letterSpacing: 0.5,
        margin: 16,
    },
    titleP: {
        marginBottom: 3, 
        letterSpacing: 6.67,
        marginBottom: 37,
        marginTop: "53%",
        textAlign: 'center',
        fontSize: 11,
        fontWeight: 'bold',
        height: 16,
        color: '#FAFAFA',
        letterSpacing: 0.8,
        fontFamily: 'FuturaBT-Medium' 
    }
})