import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { height, width } from '../constants/dimensions.js';

export default function HomeButton() {
    return (
        <TouchableOpacity style = {styles.homeButton}>
            <Text style={{textAlign: 'center', justifyContent: 'center', color: 'white', alignSelf: 'center', padding: '2%', }}>SHOP NOW</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    homeButton: {
        alignSelf: 'center',
        justifyContent: 'center',
        width: '55%',
        marginTop: 10,
        height: '8%',
        color: 'white',
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        fontFamily: "FuturaBT-Medium",

    }
})