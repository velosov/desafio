import React from 'react';
import { Image, StyleSheet, Text, SafeAreaView, View } from 'react-native';
import { height, width } from '../constants/dimensions.js';


const TextsView = () => {
    return(
        <View style={styles.textsView}>
            <Text style={{fontSize: 15, marginLeft: 2}}>Camisa de seda parka mi...</Text>
            <Text style={{fontWeight:'bold', height: 24, backgroundColor: 'transparent', marginLeft: 5,}}>R$529</Text>
            <Text style={{height: 22, marginTop: 0, marginLeft: 3}}>5x de R$105,80</Text>
        </View>
    )
}

function CardH2 ({img}) {
    switch(img){
        case 'raposa': 
            return (
                <SafeAreaView style={styles.stack}>
                    <Image source={require('../assets/Home/raposa.png')} style={styles.img} resizeMode= 'stretch' />
                    <TextsView />
                </SafeAreaView>
            )
            break;
        case 'pastel': 
            return (
                <SafeAreaView style={styles.stack}>
                    <Image source={require('../assets/Home/pastel.png')} style={styles.img} resizeMode= 'stretch' />
                    <TextsView />
                </SafeAreaView>
            )
            break;
        case 'vestido1': 
            return (
                <SafeAreaView style={styles.stack}>
                    <Image source={require('../assets/Home/vestido1.png')} style={styles.img} resizeMode= 'stretch' />
                    <TextsView />
                </SafeAreaView>
            )
            break;
        case 'vestido2': 
            return (
                <SafeAreaView style={styles.stack}>
                    <Image source={require('../assets/Home/vestido2.png')} style={styles.img} resizeMode= 'stretch' />
                    <TextsView />
                </SafeAreaView>
            )
            break;
    
}}

export default CardH2;

const styles = StyleSheet.create({
    stack: {
        marginLeft: 16,
        height: 385,
        backgroundColor: 'transparent',
        marginBottom: 15
    },
    img: {
        width: 188,
        height: 281
    },
    textsView: {
        marginTop: 18,
        width: 160,
        height: 46,
        justifyContent: 'space-between',
        fontSize: 15
    }
})