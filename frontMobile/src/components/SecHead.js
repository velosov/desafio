import React from 'react';
import { StyleSheet, Text, TouchableOpacity, SafeAreaView } from 'react-native';
import { height, width } from '../constants/dimensions.js';

export default function SecHead (props) {
    return (
        <SafeAreaView style={styles.sectionHeader}>
            <Text style={{textAlign: 'left', fontWeight: '600'}}>{props.left}</Text>
            <TouchableOpacity style={{textAlign: 'right', textTransform: 'initial'}} >
                <Text>{props.right}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
    }

const styles = StyleSheet.create({
    sectionHeader: {
        flexDirection: 'row',
        width: width * 0.9,
        height: 20,
        marginLeft: 16,
        justifyContent: 'space-between',
        textAlign: 'left',
        letterSpacing: 1.08,
        color: '#000000',
        textTransform: 'uppercase',
        opacity: 1,
        marginTop: 35,
        marginBottom: 20,
    }
})