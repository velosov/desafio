import React from 'react';
import {SvgXml} from 'react-native-svg';

export default function IconBag() {
    const svgMarkup = '<svg xmlns="http://www.w3.org/2000/svg" width="16.8" height="18.14" viewBox="0 0 16.8 18.14"><defs><style>.a,.c{fill:none;}.a{stroke:#fff;stroke-miterlimit:10;}.b{stroke:none;}</style></defs><g transform="translate(0 0.5)"><g class="a" transform="translate(0 5.04)"><path class="b" d="M0,0H16.8a0,0,0,0,1,0,0V9.6a3,3,0,0,1-3,3H3a3,3,0,0,1-3-3V0A0,0,0,0,1,0,0Z"/><path class="c" d="M1,.5H15.8a.5.5,0,0,1,.5.5V9.6a2.5,2.5,0,0,1-2.5,2.5H3A2.5,2.5,0,0,1,.5,9.6V1A.5.5,0,0,1,1,.5Z"/></g><path class="a" d="M0,8.4V3.78A3.589,3.589,0,0,1,3.36,0,3.589,3.589,0,0,1,6.72,3.78V7.56" transform="translate(5.04)"/></g></svg>';
    const SvgImage = () => <SvgXml xml={svgMarkup} width='15px' />;

    return <SvgImage/>;
}