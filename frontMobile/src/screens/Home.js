//Página de número 1 do layout disponibilizado no Xd

import React from 'react';
import { ImageBackground, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

//import Swiper from 'react-native-swiper';
import HomeButton from '../components/HomeButton.js';
import CardH1 from '../components/CardH1.js';
import CardH2 from '../components/CardH2.js';
import SecHead from '../components/SecHead.js';

import {Entypo, EvilIcons, MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';

import { height, width } from '../constants/dimensions.js';


export default function Home({ navigation: {navigate} }) {
    
    return (
            <ScrollView>
                <View style = {styles.newWaveContainer}>
                    <ImageBackground source = {require('../assets/Home/editorial_pic.png')} style={{resizeMode: 'contain', height: 536, width: '100%'}}>
                        <Text style={{textAlign: 'center', height: 19, marginBottom: 3, letterSpacing: 6.67, marginTop: 330, fontFamily: 'Didot-Bold', color: "#FAFAFA" }}>PREVIEW</Text>
                        <Text style={{textAlign: 'center', fontSize: 40, marginBottom: 24, letterSpacing: 2.81, fontWeight: '100', fontFamily: 'Didot-Bold', color: "#FAFAFA" }}>NEW WAVE</Text>
                        <HomeButton />
                    </ImageBackground>
                </View>

                <SecHead left='NEW IN' right='Ver Tudo' />
                <View style={styles.carousel}>
                    <CardH1 title='HOT TRENDS' />
                    <CardH1 title='JAQUETAS' />
                </View>

                <CardH1 title='PREVIEW' />

                <SecHead left='VISTOS RECENTEMENTE' right='Limpar' />
                <View style={styles.carousel}>
                    <CardH2 img='raposa' />
                    <CardH2 img='pastel' />
                </View>

                <SecHead left='VOCÊ PODE CURTIR' right='' />
                <View style={styles.carousel}>
                    <CardH2 img='vestido1' />
                    <CardH2 img='vestido2' />
                </View>

                <TouchableOpacity style={styles.container}>
                    <EvilIcons name='location' size={30} color='grey' style={{justifyContent: 'flex-start', margin: 2, alignSelf: 'center'}} />
                    <Text style={styles.find}>Encontre uma loja</Text>
                    <MaterialIcons name='arrow-forward-ios' size={20} color='grey' style={{justifyContent: 'center', marginTop: 7, paddingRight: 17}} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.container}>
                    <MaterialCommunityIcons name='message-outline' size={30} color='grey' />
                    <Text style={styles.find}>Fale com um vendedor</Text>
                    <MaterialIcons name='arrow-forward-ios' size={20} color='grey' style={{justifyContent: 'center', marginTop: 7, paddingRight: 17, fontWeight: 400}} />
                </TouchableOpacity>
            </ScrollView>
        
    )
}

const styles = StyleSheet.create({
    carousel: {
        flexDirection: 'row',
        height: 385,
    },
    container: {
        alignSelf: 'center',
        flexDirection: 'row',
        borderTopWidth: 1,
        width: width*0.9,
        height: 80,
        padding:20,
        marginTop: 5
    },
    find: {
        width: 300,
        height: 80,
        fontSize: 18,
        margin: 5,
        justifyContent: 'center',
        textAlign:'left',
        paddingLeft: 10
     },
    newWaveContainer: {
        width: width,
        color: '#FAFAFA'
    },
    
})