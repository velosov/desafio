//A ideia pelo menos era retornar um cardzinho com as infos mais básicas facilmente acessáveis

import React, { useEffect, useState } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { height, width } from '../constants/dimensions.js';
import api from '../services/api.js';

const products = require('../constants/products.json');



const Card = (product) => {

    //seria usada para requisitar uma imagem à VTEX e dps renderizá-la em uma <Image/>
    function getImage(URL) {
        try {
            const response = api.get(URL);
            return(
                response.body
            );
        } catch(e) {
            console.log('Falha em obter os dados.');
            console.log(e);
        }
    }

    return(
        <TouchableOpacity style={{flexDirection: 'row', height: height*0.35, width: width * 0.8, margin: width * 0.1, borderWidth: 1, borderColor: 'rgba(255,110,0,1)'}} >
            <View style={{justifyContent: 'center'}}>
                <Text style={styles.text}>{product.productId}</Text>
                <Text style={styles.text}>{product.productName}</Text>
                <Text style={styles.text}>{product.brand}</Text>
            </View>
            <View>
                <Text>{product.description}</Text>
            </View>
                
        </TouchableOpacity>
    )
}

export default function Products({ navigation: {navigate} }) {
    
    return (
        <SafeAreaView style={{width: width, height: height, alignItems: 'center'}}>
            <Text style={{margin: 20, height: 60, fontSize: 30, fontFamily: 'FuturaBT-Medium', fontWeight: '500', }}>Breve apresentação geral</Text>
            <FlatList data={products} renderItem={({item}) => <Card product={item} /> } keyExtractor={(item) => parseInt(item.productId)} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        color: 'black',
        fontWeight: "600",
        height: height *0.05,
        width: width*0.4
    }
});