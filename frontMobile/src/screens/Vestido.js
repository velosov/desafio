//Página de número 3 do layout disponibilizado no Xd

import React from 'react';
import { ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import {width} from '../constants/dimensions.js';
const products = require('../constants/products.json');


const TextsView = () => {
    return(
        <View style={styles.textsView}>
            <Text style={{fontSize: 15, marginLeft: 2}}>Camisa de seda parka mi...</Text>
            <Text style={{fontWeight:'bold', height: 24, backgroundColor: 'transparent', marginLeft: 5,}}>R$529</Text>
            <Text style={{height: 22, marginTop: 0, marginLeft: 3}}>5x de R$105,80</Text>
        </View>
    )
}

const CardV1 = (img) => {
    return(
        <SafeAreaView style={styles.stack}>
            <Image source={img} style={styles.image} resizeMode= 'stretch' />
            <TextsView />
        </SafeAreaView>
    )
}

const StaticCount = () => {
    return(
        <View style={styles.meioCampo}>
            <Text style={{textAlign: 'left', fontWeight: '600', fontSize: 16, height: 40, padding: 10, justifyContent:'center'}}>200 produtos</Text>
        </View>
    )
}

const Buttons = () => {
    return (
        <>
            <TouchableOpacity style={styles.button}>
                <Text style={styles.txt}>Filtrar...</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}>
                <View style={{height: '100%', width: '100%', flexDirection: 'row'}}>
                    <Text style={styles.txt}>Ordenar...</Text>
                    <MaterialIcons name='keyboard-arrow-down' size={22} style={{alignSelf: 'flex-end', padding: 11, marginLeft: 10}}/>
                </View>
            </TouchableOpacity>
        </>
    )
}


const imgList = [
    {img: require('../assets/Vestidos/EsqSup.png')},
    {img: require('../assets/Vestidos/DirSup.png')},
    {img: require('../assets/Vestidos/EsqInf.png')},
    {img: require('../assets/Vestidos/DirInf.png')}
    ];


export default function Vestido() {

    return(
        <SafeAreaView>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center', margin: 10}}>
                <Buttons/>
            </View>
            <StaticCount/>
            <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                {imgList.map((img)=>{<CardV1 img={img} /> })}
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    meioCampo: {
        height: 16,
        marginTop: 27,
        marginBottom: 20,
        marginLeft: 16,
        flexDirection:'row',
    },
    button: {
        height: 44,
        width: 169,
        backgroundColor: 'rgba(0,0,0,0.05)',
        margin: 5
    },
    txt: {
        fontSize: 16,
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 11,
        marginLeft: 12,
        marginTop: 2
    },
    stack: {
        marginLeft: 16,
        height: 385,
        backgroundColor: 'transparent',
        marginBottom: 15
    },
    image: {
        width: width*0.4,
        height: 581
    },
    textsView: {
        marginTop: 18,
        width: 160,
        height: 46,
        justifyContent: 'space-between',
        fontSize: 15
    }
})