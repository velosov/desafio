//Página de número 2 do layout disponibilizado no Xd

import React, {useState} from 'react';
import { Image, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';

import { height, width } from '../constants/dimensions.js';

import {Entypo} from '@expo/vector-icons';

//listas associando imagens aos respectivos títulos; inputs para renderizar declarativamente
const storiesList = [
    {texto: 'Malha', imagem: require('../assets/Menu/Oval.png'), },
    {texto: 'Jaquetas', imagem: require('../assets/Menu/Jaquetas.png') },
    {texto: 'Tricot', imagem: require('../assets/Menu/Tricot.png') },
    {texto: 'Blusas', imagem: require('../assets/Menu/Blusas.png') },
    {texto: 'Shop', imagem: require('../assets/Menu/Shop.png'), },
];

const titlesList = [
    {texto: 'COLEÇÃO', imagem: require('../assets/Menu/colecao.png'), },
    {texto: 'NOVIDADES', imagem: require('../assets/Menu/novidades.png') },
    {texto: 'ACESSÓRIOS', imagem: require('../assets/Menu/acessorios.png') },
    {texto: 'INTIMATES', imagem: require('../assets/Menu/intimates.png') },
    {texto: 'SALE', imagem: require('../assets/Menu/sales.png'), },
];

const Pesquisa = () => {
    return (
        <TextInput value={pesquisa} onChange={e => setPesquisa(e)} style={{ width: 333, height: 44, alignSelf: 'center', backgroundColor: "rgba(0, 0, 0, 0.05)", margin: 10, paddingLeft: 5,}} placeholder='Busque por produtos...' placeholderTextColor='black' >
            <Entypo name='magnifying-glass' size={24} color='grey' />
        </TextInput>
            )
}

const Story = ({imagem, texto}) => {
    return(
        <TouchableOpacity>
            <Image source={imagem} resizeMode='stretch' style={{ height: 64, width: 64, margin: 8, }}></Image>
            <Text style={{ fontSize: 13, fontFamily: 'FuturaBT-Book', height: 23, width: 64, margin: 8, textAlign: 'center' }}>{texto}</Text>
        </TouchableOpacity>
    )
}

const Box = ({texto, imagem, navigate}) => {
    return(
        <TouchableOpacity onPress={()=>{navigate('vestido')}} style={{ alignSelf: 'center', flexDirection: 'row', backgroundColor: 'rgba(0, 0, 0, 0.20)', width: 365, height: 104, }}>
            <Text style={{ marginTop: 5, justifyContent: 'flex-start', width: 160, fontSize: 18, fontWeight: '600', textAlign: 'left',  color: '#FAFAFA', padding: 30 }}>{texto}</Text>
            <Image style={{ justifyContent: 'flex-end' }} resizeMode='stretch' source={imagem}></Image>
        </TouchableOpacity>
    )
}

const Oro = () => {
    return(
        <TouchableOpacity style={{ flexDirection: 'row', width: 359, height: 104, }}>
            <Image style={{ }} resizeMode='contain' source={require('../assets/Menu/oro.png')}/>
            <Text style={{height: 16, width: 143, alignSelf: 'center', justifyContent:'center', textAlign: 'left', color: '#FAFAFA'}}>ANIMALE ORO</Text>
        </TouchableOpacity>
    )
}

export default function Menu({ navigation: {navigate} }) {
    const [ pesquisa, setPesquisa ] = useState('');

    function handlePesquisa() {
        console.log('Ainda em desenvolvimento');
    }

    return(
        <ScrollView style={{width: width}}>
            <TextInput value={pesquisa} onChange={e => setPesquisa(e)} style={{ width: 333, height: 44, alignSelf: 'center', backgroundColor: "rgba(0, 0, 0, 0.05)", margin: 10, paddingLeft: 5,}} placeholder='Busque por produtos...' placeholderTextColor='black' >
                <Entypo name='magnifying-glass' size={24} color='grey' />
            </TextInput>
            <View style={{ flexDirection: 'row', marginLeft: 8 }}>
                { storiesList.map((box) => <Story texto={box.texto} imagem={box.imagem} />) }
            </View>
            <View style = {{ alignItems: 'center', height: 854, justifyContent: 'space-between' }} >
                { titlesList.map((box) => <Box navigate={navigate} texto={box.texto} imagem={box.imagem} />) }
                <Oro/>
                
                <TouchableOpacity style={{ alignSelf:'center', width: 359, height: 181 }}>
                    <Image source={require('../assets/Menu/inside.png')} />
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}
