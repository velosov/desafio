import React from 'react';
import { ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { height, width } from '../constants/dimensions.js';
import {AntDesign, Entypo, MaterialIcons} from '@expo/vector-icons';


const Socials = () => {
    return(
        <View style={styles.SSOs}>
            <TouchableOpacity>
                <Entypo name='facebook' size={40} />
            </TouchableOpacity>
            <TouchableOpacity>
                <AntDesign name='instagram' size={40} />
            </TouchableOpacity>
            <TouchableOpacity>
                <AntDesign name='github' size={40} />
            </TouchableOpacity>
            <TouchableOpacity>
                <AntDesign name='slack' size={40} />
            </TouchableOpacity>
            <TouchableOpacity>
                <AntDesign name='google' size={40} />
            </TouchableOpacity>
        </View>
            )
}

export default function Conta({ navigation: {navigate} }) {
    
    return (
        <SafeAreaView style={styles.container}>
            <MaterialIcons name='developer-mode' size={150} color='black' />
            <Text style = {{ fontSize: 22, margin:20, textAlign: 'center', width: width * 0.8, alignSelf: 'center', height: 50, fontFamily: 'FuturaBT-Medium' }}>Faça login para continuar</Text>
            <Socials/>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        width: width,
        height: height,
        alignItems:'center',
        justifyContent:'space-evenly',
        marginTop: -40
    },
    SSOs: {
        flexDirection: 'row',
        width: width,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
})