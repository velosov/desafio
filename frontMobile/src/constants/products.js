//Filtra e onverte os dados de products.json para renderização

var products = require('./products.json');

const produtos = [];

for (let product in products) {
    produtos.push({
        title: product.productName,
        brand: product.brand,
        description: product.description,
        code: product.productId,
        //imageURL: product.items.images.imageURL,
        //price: product.sellers[0].commertialOffer.Installments[0].Value,
    });
}

export default produtos;