import React, { useEffect, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { NavigationContainer, NavigationHelpersContext } from "@react-navigation/native";
import { createStackNavigator, HeaderTitle } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";


//import './assets/fonts/futura-bt.css';
import {
  AntDesign,
  Feather,
  Ionicons,
  SimpleLineIcons
} from "@expo/vector-icons";

//Bloco de importação do SVG disponível no Xd, que lamentavelmente não rodou:
//import IconBag from './components/IconBag.js';
//import IconHome from './components/IconHome.js';
//import IconMenu from './components/IconMenu.js';
//import IconAccount from './components/IconSaved.js';

import Home from './screens/Home.js';
import Menu from './screens/Menu.js';
import Products from './screens/Products.js';
import Conta from './screens/Conta.js';
import Vestido from './screens/Vestido.js';



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function Flow({ navigation }) {
  // navegação entre telas empilhadas

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="mainNav"
        component={Tabflow}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="vestido"
        component={Vestido}
        options={{
          headerTitleAlign: "center",
          title: "VESTIDO",
          headerTitleStyle: {
            fontWeight: "600",
          },
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                height: 24,
                width: 24,
                marginVertical: 3,
                marginHorizontal: 11,
              }}
            >
              <AntDesign name="left" size={24} color="black" />
            </TouchableOpacity>
          ),
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                height: 24,
                width: 24,
                marginVertical: 3,
                marginHorizontal: 11,
              }}
            >
              <SimpleLineIcons name="handbag" size={24} color="black" />
            </TouchableOpacity>
          ),
        }}
      />
    </Stack.Navigator>
  );
}

function Tabflow({ navigation: {navigate} }) {
  //navegação entre as abas da barra inferior

  return (
    <Tab.Navigator
      screenOptions={{
        
        tabBarlabelStyle: { fontSize: 16, fontFamily: 'FuturaBT-Medium', },
        tabBarActiveTintColor: "rgba(255, 110, 0, 1)",
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
          tabBarLabel: "Home",
          tabBarIcon: ({ focused }) => (
            <Feather name='home' size={30} color={focused? 'rgba(255, 110, 0, 1)' : 'rgba(0, 0, 0, 0.5)' } style={{marginTop:7}}/>
          ),
        }}
      />
      <Tab.Screen
        name="Menu"
        component={Menu}
        options={{
          title: 'MENU',
          headerRight: () => (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                height: 24,
                width: 24,
                marginVertical: 3,
                marginHorizontal: 11,
              }}
            >
              <SimpleLineIcons name="handbag" size={24} color="black" />
            </TouchableOpacity>
          ),
          headerTitleStyle: {letterSpacing: 1.56, fontFamily: 'FuturaBT-Medium'},
          tabBarLabel: "Menu",
          tabBarIcon: ({ focused }) => (
            <AntDesign name='bars' size={30} color={focused? 'rgba(255, 110, 0, 1)' : 'rgba(0, 0, 0, 0.5)' } style={{marginTop:7}} />
          ),
        }}
        />
     
     <Tab.Screen
        name="My List"
        component={Products}
        options={{
          headerShown: false,
          tabBarLabel: "My List",
          tabBarIcon: ({ focused }) => (
            <Feather name='bookmark' size={30} color={focused? 'rgba(255, 110, 0, 1)' : 'rgba(0, 0, 0, 0.5)' } style={{marginTop:7}} />
          ),
        }}
        />
      
      <Tab.Screen
        name="Conta"
        component={Conta}
        options={{
          headerShown: false,
          tabBarLabel: "Conta",
          tabBarIcon: ({ focused }) => (
            <Ionicons name='person-outline' size={30} color={focused? 'rgba(255, 110, 0, 1)' : 'rgba(0, 0, 0, 0.5)' } style={{marginTop:7}} />
          ),
        }}
        />
    </Tab.Navigator>
  );
}


export default function Routes() {

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={ {headerShown: false} }
        initialRouteName={"Inicio"}
      >
        <Stack.Screen name="Inicio" component={Flow} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}