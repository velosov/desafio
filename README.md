# Desafio Desenvolvedor App

## Instruções
O Projeto foi desenvolvido com Expo, que oferece uma CLI e um bundler (como o Webpack), além de outras comodidades. Para rodar localmente 
fazendo seu uso basta comandar npm/expo start após instalar as dependências e abrir a aplicação em um dispositivo mobile (iOS ou Android)
pelo app Expo Go ou ainda em algum emulador destes softwares para Desktop.

Agradeço novamente pela oportunidade, em especial pela extensão de prazo do fim de semana; sem ela eu não teria conseguido entregar um 
app minimamente navegável. Espero que nos vejamos novamente em breve. 


Aguardo retorno e permaneço à disposição em viniciusveloso@poli.ufrj.br ou +55 21 96717-2530. Abs!